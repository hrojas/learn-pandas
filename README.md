Place for everything Pandas.

How to install Pandas?
-------

* [Windows](http://www.youtube.com/watch?v=g4v9_K3Rq3Y)

Lessons
-------

* [01 - Lesson](https://nbviewer.org/url/bitbucket.org/hrojas/learn-pandas/raw/master/lessons/01%20-%20Lesson.ipynb)
* [02 - Lesson](https://nbviewer.org/url/bitbucket.org/hrojas/learn-pandas/raw/master/lessons/02%20-%20Lesson.ipynb)
* [03 - Lesson](https://nbviewer.org/url/bitbucket.org/hrojas/learn-pandas/raw/master/lessons/03%20-%20Lesson.ipynb)
* [04 - Lesson](https://nbviewer.org/url/bitbucket.org/hrojas/learn-pandas/raw/master/lessons/04%20-%20Lesson.ipynb)
* [05 - Lesson](https://nbviewer.org/url/bitbucket.org/hrojas/learn-pandas/raw/master/lessons/05%20-%20Lesson.ipynb)
* [06 - Lesson](https://nbviewer.org/url/bitbucket.org/hrojas/learn-pandas/raw/master/lessons/06%20-%20Lesson.ipynb)
* [07 - Lesson](https://nbviewer.org/url/bitbucket.org/hrojas/learn-pandas/raw/master/lessons/07%20-%20Lesson.ipynb)
* [08 - Lesson](https://nbviewer.org/url/bitbucket.org/hrojas/learn-pandas/raw/master/lessons/08%20-%20Lesson.ipynb)
* [09 - Lesson](https://nbviewer.org/url/bitbucket.org/hrojas/learn-pandas/raw/master/lessons/09%20-%20Lesson.ipynb)
* [10 - Lesson](https://nbviewer.org/url/bitbucket.org/hrojas/learn-pandas/raw/master/lessons/10%20-%20Lesson.ipynb)
* [11 - Lesson](https://nbviewer.org/url/bitbucket.org/hrojas/learn-pandas/raw/master/lessons/11%20-%20Lesson.ipynb)


Cookbooks
---------

* [Compute](https://nbviewer.org/url/bitbucket.org/hrojas/learn-pandas/raw/master/lessons/Cookbook%20-%20Compute.ipynb)
* [Merge](https://nbviewer.org/url/bitbucket.org/hrojas/learn-pandas/raw/master/lessons/Cookbook%20-%20Merge.ipynb)
* [Select](https://nbviewer.org/url/bitbucket.org/hrojas/learn-pandas/raw/master/lessons/Cookbook%20-%20Select.ipynb)
* [Sort](https://nbviewer.org/url/bitbucket.org/hrojas/learn-pandas/raw/master/lessons/Cookbook%20-%20Sort.ipynb)
* [Python 101](https://nbviewer.org/url/bitbucket.org/hrojas/learn-pandas/raw/master/lessons/Python_101.ipynb)

